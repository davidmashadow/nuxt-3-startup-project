export default defineNuxtConfig({
  app: {
    head: {},
  },

  typescript: { shim: false, strict: true },

  experimental: { reactivityTransform: true },

  modules: ['@vueuse/nuxt', '@pinia/nuxt', 'nuxt-icon', '@nuxtjs/tailwindcss'],

  build: {
    transpile: ['@heroicons/vue']
  }

})
