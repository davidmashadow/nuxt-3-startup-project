# The builder stage
# This stage is responsible for:
# 1. Installing the necessary dependencies
# 2. Building the NuxtJS application
# 3. Bundling the necessary external packages into a standalone build

FROM node:18.12.1-alpine as builder

# Create a place in the container to process the NuxtJS application in
RUN mkdir -p /usr/src/nuxt3-app
WORKDIR /usr/src/nuxt3-app

# Copy all the files (excluding those defined in the .dockerignore file)
COPY . .

# Build the NuxtJS application, including devDependencies and create a production build
RUN npm install --location=global npm@9.2.0
RUN npm install --location=global pnpm
RUN pnpm install
RUN pnpm nuxi build
RUN ls -all

# Build the NuxtJS application in production mode
RUN rm -rf node_modules && \
pnpm install --ignore-scripts

# The runner stage
# This is the final image that will be used when running the Docker container
# It's responsible for:
# 1. Copying the necessary custom runtime files to the container
# 2. Copying the NuxtJS configuration
# 3. Copying the entire NuxtJS application (the .nuxt, .output and static folders)
# 4. Setting container variables
# 5. Defining the start-up command

FROM node:18.12.1-alpine as runner

# Create a separate folder for the application to live in
WORKDIR /usr/src/nuxt3-app

# Copy libs and prune the source
# COPY --from=builder /app/libs ./libs

# Copy the NPM modules
COPY --from=builder /usr/src/nuxt3-app/node_modules ./node_modules

# Copy the custom runtime files from the builder
# COPY --from=builder /app/modules ./modules
# COPY --from=builder /app/middleware ./middleware

# Copy the NuxtJS configuration for start-up and runtime configuration settings from the builder
COPY --from=builder /usr/src/nuxt3-app/nuxt.config.ts ./nuxt.config.ts

# Copy the entire NuxtJS application from the builder
COPY --from=builder /usr/src/nuxt3-app/.nuxt ./.nuxt
COPY --from=builder /usr/src/nuxt3-app/.output ./.output
# COPY --from=builder /app/static ./static

# Expose port 3000 for the docker containers
EXPOSE 3000

# Set NuxtJS system variables so the application can be reached on your network
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

# Start the NuxtJS application through a Node CLI command to launch and serve the built application
CMD [ "node", "./.output/server/index.mjs" ]
